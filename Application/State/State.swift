//
//  State.swift
//  boiler2
//
//  Created by Louis Foster on 12/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation

protocol StateReferenceProtocol
{
    var delegate: StateProtocol { get }
}

protocol EntityProtocol: StateReferenceProtocol
{
    var id: Int { get }
}

protocol StateProtocol
{
    var entities: [ EntityProtocol ] { get }
    var logger: LoggerProtocol { get }
    
    func log( _ text: String )
}
