//
//  AppDelegate.swift
//  boiler2
//
//  Created by Louis Foster on 10/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate
{
    var mainApplication: Main?
    
    func applicationDidFinishLaunching( _ aNotification: Notification )
    {
        self.mainApplication = Main( )
    }

    func applicationWillTerminate( _ aNotification: Notification ) { }
}

