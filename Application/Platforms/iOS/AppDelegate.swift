//
//  AppDelegate.swift
//  boiler2
//
//  Created by Louis Foster on 10/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?

    func application( _ application: UIApplication,
                      didFinishLaunchingWithOptions launchOptions: [
                          UIApplicationLaunchOptionsKey: Any
                      ]? ) -> Bool
    {
        /*
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        self.mainViewController = MainViewController()
        
        self.window?.rootViewController = self.mainViewController
        
        self.window?.makeKeyAndVisible()
        */
        
        return true
    }

    func applicationWillResignActive( _ application: UIApplication ) { }

    func applicationDidEnterBackground( _ application: UIApplication ) { }

    func applicationWillEnterForeground( _ application: UIApplication ) { }

    func applicationDidBecomeActive( _ application: UIApplication ) { }

    func applicationWillTerminate( _ application: UIApplication ) { }
}

