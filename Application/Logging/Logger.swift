//
//  Logger.swift
//  boiler2
//
//  Created by Louis Foster on 12/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

/*
 
 Logger is the primary logging system for this app.
 It is used by a state manager to keep track of all
 occurrences and errors within the app.
 
 TODO: save output to file
 
 */

import Foundation

protocol LoggerProtocol
{
    var history: [ LogItem ] { get }
    
    /// Push log into history and output to console
    /// Log is of type LogItem and includes timestamp
    ///
    /// - Parameter text: Text to be logged
    mutating func push( _ text: String )
    
    /// Returns the most recently log item
    ///
    /// - Returns: Log Item data in string format
    func last( ) -> String
    
    /// Returns all log items in history
    ///
    /// - Returns: All log item data in a single string
    func all( ) -> String
}

struct LogItem
{
    var item: String
    var date: Date
    
    func toString( format: DateFormatter ) -> String
    {
        var value: String = ""
        value += "\( self.item )"
        value += " @ "
        value += "\( format.string( from: self.date ) )"
        
        return value
    }
}

struct Logger: LoggerProtocol
{
    private(set) var history: [ LogItem ]
    
    init( )
    {
        self.history = [ ]
        self.push( "Logger created" )
    }
    
    mutating func push( _ text: String )
    {
        self.history.append( LogItem( item: text, date: Date( ) ) )
        print( self.last( ) )
    }
    
    func last( ) -> String
    {
        guard let current = self.history.last
        else
        {
            return "Nothing logged"
        }
        
        let dateFormatter = DateFormatter( )
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium
        dateFormatter.locale = Locale(identifier: "en_US")
        
        return "[\( self.history.count - 1 )]: \(current.toString( format: dateFormatter ) )"
    }
    
    func all( ) -> String
    {
        if self.history.count == 0
        {
            return "Nothing logged"
        }
        
        let dateFormatter = DateFormatter( )
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        dateFormatter.locale = Locale(identifier: "en_US")
        
        var output: String = ""
        
        for index in 0..<self.history.count
        {
            var value: String = "[\( index )]: "
            value += self.history[index].toString( format: dateFormatter )
            value += "\n"
            
            output += value
        }
        
        return output
    }
}
