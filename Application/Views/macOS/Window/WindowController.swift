//
//  WindowController.swift
//  macOS
//
//  Created by Louis Foster on 12/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Cocoa

protocol WindowControllerProtocol: EventListenerProtocol { }

class WindowController: NSWindowController, WindowControllerProtocol
{
    // MARK: Properties
    
    var id: Int
    
    var delegate: StateProtocol
    
    override var windowNibName: NSNib.Name?
    {
        return NSNib.Name( "WindowController" )
    }
    
    // MARK: Initialiser
    
    required init?(coder: NSCoder)
    {
        fatalError( "init(coder:) has not been implemented. Use init()" )
    }
    
    init( id _id: Int, delegate _delegate: StateProtocol )
    {
        self.id = _id
        self.delegate = _delegate
        self.delegate.log( "Window with id \( self.id ) was created" )
        super.init( window: nil )
    }
    
    // MARK: Window life cycle
    
    override func windowDidLoad( )
    {
        super.windowDidLoad( )
        self.delegate.log( "Window with id \( self.id ) loaded" )
        
        self.contentViewController = MacOSMetalViewController( )
    }
    
    // MARK: Window actions
    
    private func rotateWindow( ) throws
    {
        guard let window = self.window
        else
        {
            throw WindowError.noWindow
        }
        
        let currentPosition = window.frame.origin
        let rect = NSRect( x: currentPosition.x,
                           y: currentPosition.y,
                           width: window.frame.height,
                           height: window.frame.width )
        window.setFrame( rect, display: true )
        
        self.delegate.log( "Window with id \( self.id ) rotated" )
    }
}

// MARK: - EventListenerProtocol methods
extension WindowController
{
    func run( event _index: Int )
    {
        switch _index
        {
        case 0:
            do
            {
                try self.rotateWindow( )
            }
            catch WindowError.noWindow
            {
                self.delegate.log( WindowError.noWindow.message )
            }
            catch
            {
                self.delegate.log( GenericError.unknown.message )
            }
        default:
            return
        }
    }
    
    func listEvents() -> [String]
    {
        return [
            "Rotate Window",
        ]
    }
}
