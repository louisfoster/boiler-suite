//
//  MacOSMetalViewController.swift
//  macOS
//
//  Created by Louis Foster on 12/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Cocoa

protocol MacOSMetalViewControllerProtocol { }

class MacOSMetalViewController: MetalViewController, MacOSMetalViewControllerProtocol
{
    // MARK: Properties
    
    override var acceptsFirstResponder : Bool
    {
        // Allow view to receive keypress (remove the purr sound)
        return true
    }
    
    // MARK: View life cycle
    
    override func viewDidLoad( )
    {
        super.viewDidLoad( )
    }
}
