//
//  MetalViewController.swift
//  boiler2
//
//  Created by Louis Foster on 10/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

/*
 
 The purpose of this class is to do the initial view
 setup and ensure an instance of metal is available
 to the app. The valid metal device is then used to
 create the renderer, which takes it from there.
 
 This view current handles key input events for macOS.
 This might change as classes are abstracted to handle
 more gestures and complex views.
 
 */

#if os( iOS )
import UIKit
typealias PlatformViewController = UIViewController
#else
import Cocoa
typealias PlatformViewController = NSViewController
#endif

import MetalKit

protocol MetalViewControllerProtocol: class
{
//    var renderer: RendererProtocol? { get }
    
    func setupMTK( ) throws
    func setupWithPrintErrors( )
}

class MetalViewController: PlatformViewController, MetalViewControllerProtocol
{
    // MARK: Properties
    
//    private(set) var renderer: RendererProtocol?
    
    // MARK: Initialization
    
    func setupMTK( ) throws
    {
        guard let _mtkView = self.view as? MTKView
            else
        {
            throw MetalViewError.noView
        }
        
        _mtkView.device = MTLCreateSystemDefaultDevice( )
        
//        self.renderer = try Renderer( _mtkView )
        
//        _mtkView.delegate = self.renderer
    }
    
    func setupWithPrintErrors( )
    {
        do
        {
            try self.setupMTK( )
        }
        catch MetalViewError.noView
        {
            print( "No view found during setup." )
        }
        catch MetalViewError.noDevice
        {
            print( "No device found during setup." )
        }
        /*
        catch RendererInitError.noPipelineState
        {
            print( "No pipeline state found during setup." )
        }
        catch RendererInitError.noCommandQ
        {
            print( "No command queue found during setup." )
        }
        catch RendererInitError.noObject
        {
            print( "No object found during setup." )
        }
        */
        catch
        {
            print( "Unknown setup error." )
        }
    }
    
    // MARK: - View Controller Life Cycle
    
    override func viewDidLoad( )
    {
        super.viewDidLoad( )
        
        self.setupWithPrintErrors( )
    }
}
