//
//  InputHandler.swift
//  macOS
//
//  Created by Louis Foster on 12/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

/*
 
 Instantiated from the main app class
 Utilises the state delegate to manage event propogation
 
 */

import Cocoa

enum InputEvent
{
    case commandAndChar(String)
}

protocol InputHandlerDelegate: StateProtocol
{
    /// Entities listening to input char with command modifier
    var commandAndCharEvents: [ String: [ ( entity: EventListenerProtocol,
                                            runIndex: Int ) ] ] { get set }
    
    /// Taking an input char, run the events listening for this input
    /// when the command key is being pressed
    ///
    /// - Parameter char: input event character
    /// - Throws: throws an input error if char is not handled
    func onInputCommandAndChar( with char: String ) throws
}

protocol InputHandlerProtocol: StateReferenceProtocol
{
    /// On a key down event (incl. w/ modifiers) handles
    /// the calling of the state delegate and correct funcs
    ///
    /// - Parameter event: Input event
    /// - Returns: Returns bool as to whether input is handled
    func handleKeyDown( with event: NSEvent ) -> Bool
}

class InputHandler: InputHandlerProtocol
{
    // MARK: Properties
    
    var delegate: StateProtocol
    
    // MARK: Initialisers
    
    init( delegate _delegate: InputHandlerDelegate )
    {
        self.delegate = _delegate
        self.delegate.log( "Created Input Handler" )
        
        // Listen for key modifiers (such as command, shift, etc)
        // This is handled during other key events
        NSEvent.addLocalMonitorForEvents( matching: .flagsChanged )
        {
            return $0
        }
        
        // Listen for the "key down" action and call associated method
        NSEvent.addLocalMonitorForEvents( matching: .keyDown )
        {
            if self.handleKeyDown(with: $0)
            {
                return nil
            }
            else
            {
                return $0
            }
        }
    }
    
    // MARK: Delegate methods
    
    private func onCommandAndChar( _ char: String ) -> Bool
    {
        self.delegate.log( "Input event has characters: \( char )" )
        
        guard let _delegate = self.delegate as? InputHandlerDelegate
        else
        {
            self.delegate.log( "Could not get delegate for Input Handler" )
            return false
        }
        
        do
        {
            try _delegate.onInputCommandAndChar( with: char )
            return true
        }
        catch InputError.inputEventNotFound
        {
            self.delegate.log( InputError.inputEventNotFound.message )
        }
        catch
        {
            self.delegate.log( GenericError.unknown.message )
        }
        
        return false
    }
    
    // MARK: Input handlers
    
    func handleKeyDown( with event: NSEvent ) -> Bool
    {
        self.delegate.log( "An input event occurred" )
        
        switch event.modifierFlags.intersection( .deviceIndependentFlagsMask )
        {
        case [ .command ]:
            if let char = event.characters
            {
                return self.onCommandAndChar( char )
            }
            
            return false
        default:
            break
        }
        
        return false
    }
}
