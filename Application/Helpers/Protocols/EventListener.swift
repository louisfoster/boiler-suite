//
//  EventListener.swift
//  boiler2
//
//  Created by Louis Foster on 12/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

/*
 
 Entities that listen for events should implement
 this protocol
 
 */

import Foundation

protocol EventListenerProtocol: EntityProtocol
{
    /// Any entity conforming to this protocol
    /// must return an iterable list of the events
    /// that it implements
    ///
    /// - Returns: Iterable list of functions
    func listEvents( ) -> [ String ]
    
    
    /// Passing the index id of the 
    ///
    /// - Parameter _index: The index of the function to run
    func run( event _index: Int )
}
