//
//  WindowError.swift
//  macOS
//
//  Created by Louis Foster on 12/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation

enum WindowError: Error
{
    case noWindow
    
    var message: String
    {
        switch self
        {
        case .noWindow: return "No window found in window controller"
        }
    }
}
