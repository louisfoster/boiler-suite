//
//  GenericError.swift
//  boiler2
//
//  Created by Louis Foster on 12/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation

enum GenericError: Error
{
    case unknown
    
    var message: String
    {
        switch self
        {
        case .unknown: return "Unknown error :("
        }
    }
}
