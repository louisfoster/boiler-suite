//
//  InputError.swift
//  boiler2
//
//  Created by Louis Foster on 12/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation

enum InputError: Error
{
    case inputEventNotFound
    
    var message: String
    {
        switch self
        {
        case .inputEventNotFound: return "Unknown input event"
        }
    }
}
