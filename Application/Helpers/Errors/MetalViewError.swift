//
//  MetalViewErrors.swift
//  boiler2
//
//  Created by Louis Foster on 12/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation

enum MetalViewError: Error
{
    case noView
    case noDevice
    
    var message: String
    {
        switch self
        {
        case .noDevice: return "No metal device found"
        case .noView: return "No metal view found"
        }
    }
}
