//
//  Main.swift
//  boiler2
//
//  Created by Louis Foster on 12/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

/*
 
 Main is where the state is being managed from.
 It will also be where init data is consumed and exported.
 From the init data we create all necessary entities.
 
 Main also instantiates the input handler.
 
 */

import Foundation

protocol MainProtocol: InputHandlerDelegate { }

class Main: MainProtocol, StateProtocol
{
    // MARK: Properties
    
    var logger: LoggerProtocol
    var entities: [ EntityProtocol ]
    var commandAndCharEvents: [ String: [ ( entity: EventListenerProtocol,
                                            runIndex: Int ) ] ]
    
    // MARK: Initialisers
    
    init( )
    {
        self.logger = Logger( )
        self.entities = [ ]
        self.commandAndCharEvents = [ String: [ ( entity: EventListenerProtocol,
                                                  runIndex: Int ) ] ]( )
        
        _ = InputHandler( delegate: self )
        
        self.windower( )
    }
    
    // Testing create window
    func windower( )
    {
        let windowId = self.entities.count
        let newWindow = WindowController( id: windowId, delegate: self )
        self.entities.append( newWindow )
        newWindow.showWindow( nil )
        
        self.commandAndCharEvents[ "r" ] = [ ( newWindow, 0 ) ]
    }
}

// MARK: - StateProtocol methods
extension Main
{
    func log(_ text: String)
    {
        self.logger.push( text )
    }
}

// MARK: - InputHandlerDelegate methods
extension Main
{
    func onInputCommandAndChar( with char: String ) throws
    {
        guard let events = self.commandAndCharEvents[ char ]
        else
        {
            throw InputError.inputEventNotFound
        }
        
        for event in events
        {
            event.entity.run( event: event.runIndex )
        }
    }
}

